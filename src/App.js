import React from 'react';
import logo from './logo.svg';
import './App.css';

const styles = {
	h1: {
		background: "cadetblue",
		color: "azure",
		paddingBottom: "1rem",
		borderRadius: "4px",
		paddingTop: ".5rem"
	}
}

function App() {

	function Items() {
		const data = [
			{
				name: "PUMA - M Nrgy Comet",
				imgSrc: "https://www.stadium.se/INTERSHOP/static/WFS/Stadium-SwedenB2C-Site/-/Stadium/sv_SE/Detail/277040_101_PUMA_M%20NRGY%20COMET.png"
			},
			{
				name: "NIKE - Air Zoom Pegasus 36 Shield",
				imgSrc: "https://www.stadium.se/INTERSHOP/static/WFS/Stadium-SwedenB2C-Site/-/Stadium/sv_SE/Detail/292695_101_NIKE_AIR%20ZOOM%20PEGASUS%2036%20SHIELD.png"
			},
			{
				name: "NIKE - Nike Air Zoom Pegasus 36",
				imgSrc: "https://www.stadium.se/INTERSHOP/static/WFS/Stadium-SwedenB2C-Site/-/Stadium/sv_SE/Detail/282111_107_NIKE_NIKE%20AIR%20ZOOM%20PEGASUS%2036.png"
			},
			{
				name: "NIKE - Nike Legend React 2 Shield",
				imgSrc: "https://www.stadium.se/INTERSHOP/static/WFS/Stadium-SwedenB2C-Site/-/Stadium/sv_SE/Detail/292696_101_NIKE_NIKE%20LEGEND%20REACT%202%20SHIELD.png"
			},
			{
				name: "PUMA - M Nrgy Comet",
				imgSrc: "https://www.stadium.se/INTERSHOP/static/WFS/Stadium-SwedenB2C-Site/-/Stadium/sv_SE/Detail/277040_101_PUMA_M%20NRGY%20COMET.png"
			},
			{
				name: "NIKE - Air Zoom Pegasus 36 Shield",
				imgSrc: "https://www.stadium.se/INTERSHOP/static/WFS/Stadium-SwedenB2C-Site/-/Stadium/sv_SE/Detail/292695_101_NIKE_AIR%20ZOOM%20PEGASUS%2036%20SHIELD.png"
			},
			{
				name: "PUMA - M Nrgy Comet",
				imgSrc: "https://www.stadium.se/INTERSHOP/static/WFS/Stadium-SwedenB2C-Site/-/Stadium/sv_SE/Detail/277040_101_PUMA_M%20NRGY%20COMET.png"
			},
			{
				name: "NIKE - Air Zoom Pegasus 36 Shield",
				imgSrc: "https://www.stadium.se/INTERSHOP/static/WFS/Stadium-SwedenB2C-Site/-/Stadium/sv_SE/Detail/292695_101_NIKE_AIR%20ZOOM%20PEGASUS%2036%20SHIELD.png"
			},
			{
				name: "PUMA - M Nrgy Comet",
				imgSrc: "https://www.stadium.se/INTERSHOP/static/WFS/Stadium-SwedenB2C-Site/-/Stadium/sv_SE/Detail/277040_101_PUMA_M%20NRGY%20COMET.png"
			},
			{
				name: "NIKE - Air Zoom Pegasus 36 Shield",
				imgSrc: "https://www.stadium.se/INTERSHOP/static/WFS/Stadium-SwedenB2C-Site/-/Stadium/sv_SE/Detail/292695_101_NIKE_AIR%20ZOOM%20PEGASUS%2036%20SHIELD.png"
			},
			{
				name: "PUMA - M Nrgy Comet",
				imgSrc: "https://www.stadium.se/INTERSHOP/static/WFS/Stadium-SwedenB2C-Site/-/Stadium/sv_SE/Detail/277040_101_PUMA_M%20NRGY%20COMET.png"
			},
			{
				name: "NIKE - Air Zoom Pegasus 36 Shield",
				imgSrc: "https://www.stadium.se/INTERSHOP/static/WFS/Stadium-SwedenB2C-Site/-/Stadium/sv_SE/Detail/292695_101_NIKE_AIR%20ZOOM%20PEGASUS%2036%20SHIELD.png"
			}
		];

		return data.map((d) => {
			return (
				<div className="card-body mt-3 w-25">
					<img className="w-75 mb-3"
							 src={ d.imgSrc }
							 alt="new"
					/>
					<p>{ d.name }</p>
				</div>
			)
		});
	}


	return (
		<>
			<div className="container">
				<h1 className="text-center" style={styles.h1}>iPrice Marketplace</h1>

				<div className="d-inline-flex flex-wrap text-center">
					<Items/>
				</div>
			</div>
		</>
	);
}

export default App;
